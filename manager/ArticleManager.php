<?php
class ArticleManager extends DbManager implements CrudInterface {

    public function create($article){
       $query = $this->bdd->prepare("INSERT INTO article (nom, quantity, photo, prix) VALUES (:nom, :quantity, :photo, :prix)");
       $query->execute([
           "nom"=> $article->getNom(),
           "quantity"=> $article->getQuantity(),
           "photo"=> $article->getPhoto(),
           "prix"=> $article->getPrix()
       ]);
    }

    public function getAll(){
        $arrayOfObject = [];
        $query = $this->bdd->query("SELECT * FROM article");
        $query->execute();

        $results = $query->fetchAll();


        foreach ($results as $result){
            $arrayOfObject[] = new Article($result["id"], $result["nom"], $result["quantity"], $result["prix"], $result["photo"]);
        }

        return $arrayOfObject;
    }

    public function getOne($id){

        $query = $this->bdd->prepare("SELECT * FROM article WHERE id = :id");
        $query->execute([
            "id"=> $id
        ]);
        $result = $query->fetch();

        $object = new Article($result["id"], $result["nom"], $result["quantity"], $result["prix"], $result["photo"]);

        return $object;
    }


    public function delete($id){
        $query = $this->bdd->prepare("DELETE FROM article WHERE id = :id");
        $query->execute([
            "id"=> $id
        ]);
    }

    public function edit($article){
        $query = $this->bdd->prepare("UPDATE article SET nom = :nom, quantity = :quantity, prix = :prix, photo = :photo WHERE id = :id");
        $query->execute([
            "id"=> $article->getId(),
            "nom"=> $article->getNom(),
            "quantity" => $article->getQuantity(),
            "prix"=> $article->getPrix(),
            "photo"=> $article->getPhoto(),
        ]);

    }
}