<?php


/*
// Très important, j'ajoute dabord ma classe utilisateur puisque les classes clients et admin
// s'en serviront
require 'class/Utilisateur.php';
// J'ajoute ma classe client qui permettra de créer de nouveaux clients
require "class/Client.php";
// J'ajoute ma classe Admin qui permettre de créer des administrateurs
require "class/Admin.php";
// J'ajoute ma classe article qui permettra la création d'objet de type article
require "class/Article.php";

// Ici j'instancie un nouveau client. Il créera un Utilisateur en ajoutant les champs spécifiques à la classe Client
$client = new Client(1, "Delorme", "aureliendelorme1@gmail.com", "Aurelien", "Aurel", "password", "chien");

$admin = new Admin(1, "Admin", "admin@lodevie", "Admin", "Admin", "password");

// J'affiche mon objet de type client
var_dump($client);
// J'affiche mon objet de type Admin
var_dump($admin);

// Ci-dessous je cré 3 objets Article
$article1 = new Article(1, "Croquettes pour chien", 10, 100, "photo.png");
$article2 = new Article(1, "Croquettes pour chat", 10, 100, "photo.png");
$article3 = new Article(1, "Croquettes pour rat", 10, 100, "photo.png");

// Je les ajoute dans un tableau
$tableauArticles = [$article1, $article2, $article3];

// Je parcours ici mon tableau d'articles
foreach ($tableauArticles as $article){
    // Pour chacun des articles, j'affiche le nom de l'article
    echo($article->getNom().'<br>');
}
*/

// Pour utiliser tout cela :

// Nous aurons besoin d'une connexion à la bdd
// On fait donc un require sur notre classe DbManager
require "manager/DbManager.php";

// Nous aurons besoin de la représentation objet de notre table Category
// Nous devons donc faire un require sur la classe category
require "class/Category.php";
require "class/Article.php";
// Nous aurons besoin de notre manager afin de transformer nos requêtes SQL en objet
// Ou alors nos objet en requêtes SQL (en fonction du besoin)

require 'interfaces/CrudInterface.php';

require "manager/CategoryManager.php";
require "manager/ArticleManager.php";

// J'instancie ensuite un nouvel objet CategoryManager
// Il nous permettra de faire toutes les fonctions du CRUD sur nos objets
$categoryManager = new CategoryManager();
$categoryManager->delete(15);


$articleManager = new ArticleManager();
$article = new Article(null , "Croquettes pour chien", 10, 12, "croquettes.jpg");

$articleManager->create($article);
$articleManager->delete(10);


$article11 = $articleManager->getOne(11);

$article11->setNom("NOUVEAU PRODUIT");
$articleManager->edit($article11);


$articles = $articleManager->getAll();


var_dump($articles);





// Exemple 1 : Je souhaite créer une nouvelle category
// Je cré un nouvel objet category. Je ne lui assigne pas d'ID puisque c'est un auto-increment dans la base
// J'assigne des valeurs pour son attribut nom et son attribut ordreAffichage
$category = new Category(null, "Test", 200);
// J'appel mon manager pour lui demander de transformer mon objet en requête MySQL
$categoryManager->create($category);


// Exemple 2 : Je souhaite réccupéré tous les enregistrements de ma bdd
// J'appel mon manager et je lui demande de transformer toutes les lignes de ma BDD
// en tableau d'objet. J'assigne le retour dans la variable $categories qui sera donc un tableau d'objet
$categories = $categoryManager->getAll();


// Exemple 3 : Réccupérer un objet en fonction de son ID
// Ici j'appel mon category manager et je lui demande de retrouver l'objet qui à l'id 1 dans ma BDD
$category = $categoryManager->getOne(1);


// Exemple 4 : Suppression d'un objet en fonction de son id
// J'appel mon manager et je lui demande de supprimer la category qui a l'id 1
$categoryManager->delete(1);

// Exemple 5 : Modification d'un champs dans notre BDD (mise à jour du nom de la category qui a l'id 2)
// Je reccupére la category qui a l'ID 2
$category = $categoryManager->getOne(2);
// Je met à jour son attribut nom qui est privé via la mèthode publique setNom
$category->setNom("Nouveau nom !");
// J'appel mon manager pour lui demander de mettre à jour cette category
$categoryManager->edit($category);

?>

<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <?php
                foreach ($categories as $category){
                    echo('   <li class="nav-item active">
                <a class="nav-link" href="#">'.$category->getNom().'</span></a>
            </li>');
                }
            ?>


        </ul>
    </div>
</nav>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>
