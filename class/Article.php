<?php
    class Article{
        private $_id;
        private $_nom;
        private $_quantity;
        private $_prix;
        private $_photo;

        public function __construct($_id, $_nom, $_quantity, $_prix, $_photo)
        {
            $this->_id = $_id;
            $this->_nom = $_nom;
            $this->_quantity = $_quantity;
            $this->_prix = $_prix;
            $this->_photo = $_photo;
        }

        // Accesseurs

        public function getId()
        {
            return $this->_id;
        }


        public function setId($id)
        {
            $this->_id = $id;
        }

        public function getNom()
        {
            return $this->_nom;
        }


        public function setNom($nom)
        {
            $this->_nom = $nom;
        }


        public function getQuantity()
        {
            return $this->_quantity;
        }


        public function setQuantity($quantity)
        {
            $this->_quantity = $quantity;
        }


        public function getPrix()
        {
            return $this->_prix;
        }

        public function setPrix($prix)
        {
            $this->_prix = $prix;
        }

        public function getPhoto()
        {
            return $this->_photo;
        }

        public function setPhoto($photo)
        {
            $this->_photo = $photo;
        }


    }
?>