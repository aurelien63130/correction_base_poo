<?php
// Nous avons une classe abstraite, il sera impossible de faire un new Utilisateur
// En revanche, on pourra créer des classes qui en héritent.
abstract class Utilisateur{
    // Ci-dessous une série d'attributs privés. Ils ne seront pas accessible dans notre application.
    // Nous devrons utiliser les accesseurs
    private $_id;
    private $_nom;
    private $_email;
    private $_prenom;
    private $_username;
    private $_password;

    // Ici nous avons une mèthode magique. Elle servira a créer de nouveaux objets.
    // Elle sera appelé implicitement quand on utilisera le mot clé new.
    // Elle permettra l'initialisation d'un nouvel objet
    public function __construct($id, $nom, $email, $prenom, $username, $password){
        // Par exemple, l'attribut privé id de notre objet sera égal à l'id passé en paramètre
        $this->_id = $id;
        $this->_nom = $nom;
        $this->_email = $email;
        $this->_prenom = $prenom;
        $this->_username = $username;
        $this->_password = $password;
    }

    // Ci dessous les accesseurs
    // L'accesseur getId ici nous permet de retourner l'attribut _id de notre objet
    // Vu que cet attribut est privé, on passera par la mèthode getId qui elle est publique
    public function getId(){
        return $this->_id;
    }

    // Même principe que le getId mais avec l'attribut privé nom
    public function getNom(){
        return $this->_nom;
    }

    // On a ici un accesseur qui nous permet de mofifier la valeur d'un attribut privé
    // Il prend en paramètre la nouvelle valeur que l'on souhaite modifier
    // Il modifie l'attribut privé
    public function setNom($nom){
        $this->_nom = $nom;
    }

    public function getEmail(){
        return $this->_email;
    }

    public function setEmail($email)
    {
        $this->_email = $email;
    }

    public function getPrenom(){
        return $this->_prenom;
    }

    public function setPrenom($prenom){
        $this->_prenom = $prenom;
    }

    public function getUsername()
    {
        return $this->_username;
    }

    public function setUsername($username)
    {
        $this->_username = $username;
    }

    public function getPassword()
    {
        return $this->_password;
    }

    public function setPassword($password)
    {
        $this->_password = $password;
    }
}