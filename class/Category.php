<?php
    // Classe qui représente sous forme d'objet notre category en BDD
    class Category{
        // On retrouve toutes les colonnes sous forme d'attribut
        private $_id;
        private $_nom;
        private $_ordreAffichage;

        // On retrouve un constructeur qui nous permettra d'aller créer des objets
        public function __construct($id, $nom, $ordreAffichage){
            $this->_id = $id;
            $this->_nom = $nom;
            $this->_ordreAffichage = $ordreAffichage;
        }

        // On retrouve tous les accesseurs. Ils permettront d'aller lire ou écrire dans les propriétés de notre objet
        public function getId()
        {
            return $this->_id;
        }

        public function setId($id){
            $this->_id = $id;
        }

        public function getNom()
        {
            return $this->_nom;
        }

        public function setNom($nom)
        {
            $this->_nom = $nom;
        }

        public function getOrdreAffichage()
        {
            return $this->_ordreAffichage;
        }

        public function setOrdreAffichage($ordreAffichage)
        {
            $this->_ordreAffichage = $ordreAffichage;
        }
    }
?>