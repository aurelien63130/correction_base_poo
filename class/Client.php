<?php

// Ici la classe qui représentera un client de notre application e-commerce
// Elle héritera de tous les attributs de la classe Utilisateur
// Elle n'est pas abstraite, nous pourrons donc créer des clients dans notre application
class Client extends Utilisateur{
    // On ajoute un attribut animal favori.
    // Un client aura donc tous les attributs et les fonctions d'un Utilisateur. Il aura en plus
    // un attribut privé animal favori
    private $_animalFavori;

    // Dans le constructeur, je n'oublies pas d'appeler le constructeur de la classe parent
    // Puisque si l'on a 2 fois le même nom de méthode, la méthode de l'enfant écrase celle du parent
    public function __construct($id, $nom, $email, $prenom, $username, $password, $animalFavori){
        parent::__construct($id, $nom, $email, $prenom, $username, $password);
        $this->_animalFavori = $animalFavori;
    }

    // Toujours le même principe pour les accesseurs cf : Utilisateur.php

    public function getAnimalFavori(){
        return $this->_animalFavori;
    }

    public function setAnimalFavori($animal){
        $this->_animalFavori = $animal;
    }
}