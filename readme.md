**Recap du 26 Janvier**

- Les interets de la POO
    * Utilisation d'une méthode standard afin d'obtenir un code plus générique, plus facile à maintenir.  
    * En mode procédural, nos instructions PHP sont executées les unes à la suite des autres. En programmation orienté objet, nous avons un ensemble d'objets qui interragiront ensemble

- La notion de classe
  * Elle a des attributs et des mèthodes. Elle représente le moule qui servira à créer nos objets. 
  * Pour créer une classe en PHP, nous utilisons dans un fichier PHP le mot clé class

````php
<?php
class Category{
    // Le contenu de la classe
}
?> 
````

    
- La notion d'objet
    * Un objet contient des attributs et des méthodes. 
    * Il est décrit par une classe qui aura servi à le créer (instancier)
    * Pour créer un objet en php, on utilise le mot clé new. On pourra stocker nos objets dans des variables

````php
<?php
// Ici ma variable $categ sera égale à un nouvel objet category
$categ = new Category();
?> 
````

- La notion de visibilité
  - Permet de définir la visibilité de nos attributs et de nos méthodes (les différents endroits on nous pourrons visualiser / appeller nos attributs et méthodes)
  - Nous avons vu 3 visibilités différentes : 
        - private : Ces attributs ou fonctions seront visible seulement au sein de notre objet (dans notre classe). Les attributs et méthodes privé ne seront pas accessible ailleurs dans notre programme
        - public : A partir du moment ou l'on a instancié un nouvel objet, les attributs et méthodes publiques seront accessible dans tous le programme
        - protected : Nos attributs protected seront utilisable à l'interieur de nos objets mais aussi dans les classes qui héritent de notre objet.


````php
<?php
class Category{
    // Un attribut public 
    public $attribut1; 
    
    // Un attribut protected
    protected $attribut2;
    
    // un attribut privé 
    private $attribut3;
    
    
    // Une méthode public 
    public function sayHello(){
        // Contenu
    }
    
    // Un méthode protected
        protected function sayGoodbye(){
        // Contenu
    }
    // un méthode privé
    private function privateMethod(){
        // Contenu
    }
}
?> 
````

- Constructeur
    * Il sert à construire nos objets. Il est appelé implicitement lorsque l'on utilise le mot clé new 

````php
<?php
class Category{
    public function __construct(){
        echo('Hello world !');
    }
}

// Je vais passer dans mon constructeur et donc afficher hello world 
// On s'en sert essentiellement pour initialiser nos objets
$category = new Category();
?> 
````

- Destructeur
  * La méthode magique destruct est appelé quand on supprime un objet (en php avec unset())
  * Elle est aussi appelé implicitement en fin de script pour libérer la mémoire de notre serveur

````php
<?php
class Category{
    public function __destruct(){
        echo('Au revoir mon objet !');
    }
}

$category = new Category();

// Mon destructeur sera appelé puisque c'est la fin du script !
?> 
````
    
- Héritage
  - Il permet de limiter la duplication de code source entre 2 classes. Une classe parent pourra avoir une infinité de classe enfant
  - Une classe enfant possèdera toutes les méthodes et les attributs qui ont une visibilité public ou protected de notre classe parent.   
  - Pour faire de l'héritage en PHP, on utilise le mot clé extende 
  - Attention : Si dans une classe enfant on a une méthode qui le même nom que dans le parent, celle du parent sera écrasée SAUF si utilise le mot clé php parent::nomDeLaMethode();
  - Attention à bien avoir requis les fichiers contenant nos classes avant de l'utiliser. Parent en 1er et enfant en 2ème


````php
<?php
class ClassParent{
    public function load(){
        // Contenu
    }
}

class ClassEnfant extends ClassParent{
    // La méthode load de ma ClassEnfant écrase celle de la clase ClassParent
    public function load(){
        // En revanche j'appel la fonction load de ma ClassParent du coup pas de soucis !
        parent::load();
    }
}

$category = new Category();

// Mon destructeur sera appelé puisque c'est la fin du script !
?> 
````

- Classe abstraite
    * Classe qui ne pourra pas être instancié avec new 
    * Nous pourrons hériter de cette classe pour partager des attributs et méthodes avec des classes enfants. Sert de modèle. Les classes enfants pourront avoir leurs spécificités

````php
<?php
    abstract class ClassQuiNestPasInstanciable{
        // Contenu de la classe 
    }
    
    // IMPOSSIBLE 
    $element = new ClassQuiNestPasInstanciable();
?> 
````
**Recap du 27 Janvier**

- Interfaces 
  
  -  Contrat qui permet de lister la définition des méthodes que devra impérativement avoir une classe. Sinon on aura une erreur PHP

````php
<?php
// Liste les méthodes obligatoires (avec les différents parametres)
interface MonInterface{
    // La classe qui utilise cette interface devra obligatoirement avec une méthod method1(); sans arguments
    public function method1();
    // Toutes les classes qui utiliseront cette interface devront avoir une méthode method2($id) avec le parametre $id
    public function method2($id);
    
}

class QuiUtiliseLinterface implements MonInterface {
    // Si j'ai pas les 2 methodes : erreur !
    
    public function method1(){
        // ICI SON CONTENU
    }
    
    public function method2(){
        // Le contenu
    }
}
````

- On a la possibilité d'utiliser plusieurs interfaces sur une classe

````php
<?php

class MaClasse implements Interface1, Interface2{
    // Ici je devrais avoir les méthodes listées dans Interface1 et Interface2
}
````
  
- On peut utiliser aussi de l'héritage d'interface. 


- Erreurs et des exceptions

* Permet la gestion des erreurs d'une application
Permettre d'ajouter des erreurs propres à l'application. 
  
````php
// ICI ON LANCE UNE NOUVELLE EXCEPTION
// Exception est un objet PHP dans son contructeur on doit au moins lui passer un message 
throw new Exception("Une erreur que j'ai lancé !");

/*
 * Classe Exception PHP-8
 */
````
* Objet Exception

Il contient : 
- Un message
- Un code 
- Le fichier qui a lancé l'erreur 
- La ligne à laquelle l'erreur à été lancée 

On rettrouve les getteurs pour retrouver les valeurs de ces attributs privés. Pas de setteurs, on ne peut pas les modifier

````php
<?php
   // Classe Exception PHP8 
   class Exception implements Throwable {
    /** The error message */
    protected $message;
    /** The error code */
    protected $code;
    /** The filename where the error happened  */
    protected $file;
    /** The line where the error happened */
    protected $line;
    
        #[Pure]
    final public function getMessage() { }

    /**
     * Gets the Exception code
     * @link https://php.net/manual/en/exception.getcode.php
     * @return mixed|int the exception code as integer in
     * <b>Exception</b> but possibly as other type in
     * <b>Exception</b> descendants (for example as
     * string in <b>PDOException</b>).
     */
    #[Pure]
    final public function getCode() { }

    /**
     * Gets the file in which the exception occurred
     * @link https://php.net/manual/en/exception.getfile.php
     * @return string the filename in which the exception was created.
     */
    #[Pure]
    final public function getFile() { }

    /**
     * Gets the line in which the exception occurred
     * @link https://php.net/manual/en/exception.getline.php
     * @return int the line number where the exception was created.
     */
    #[Pure]
    final public function getLine() { }

    /**
     * Gets the stack trace
     * @link https://php.net/manual/en/exception.gettrace.php
     * @return array the Exception stack trace as an array.
     */
    #[Pure]
    final public function getTrace() { }
    
    }
   
?>

- On retrouve la trace également qui représente l'arborescence des fichiers parcouru depuil l'erreur

````

* On peut utiliser un try/catch pour executer une action spécifique en cas d'erreur

On execute une portion de code si on a une erreur dans cette portion de code on passe dans le catch
Le catch prend 1 paramètre. Notre Exception. Elle peut provenir de la classe PHP Exception ou d'une classe qui en hérite (plus précis).

Soit on applique pas le throw et le programme continu malgrès l'erreur 
Soit on fait un traitement avant de lancer l'erreur (envoi de mail ... ?)

- METHODES MAGIQUES : 
  Mèthode qui commence par '__'. Elles n'ont pas besoin d'être appelée elle sont appelés en fonction d'evenement
  On peut effectuer un traitement sur notre objet dans le corps de ces mèthodes. 
  
  - __construct
  Appelé quand on cré un nouvel objet ìl est appelé quand on utilise le mot clé new

````php
<?php

class SuperClasse{
  public function __construct(){
    echo('Un objet à été créé !');
  }
}

$sc = new SuperClasse();
````
  
  - __destruct
   * Appelé implicitement à la fin d'un script ou quand on supprime notre objet.

````php
<?php
class SuperClasse{
    public function __destruct(){
        echo('Objet détruit !');
    }
}
````

  - __get 
  * Sert à detecter l'accession à un attribut inexistant ou inaccessible (protected, private).

`````php
<?php
    class Session{
        private $attributs;
        
        public function __construct(){
            $this->attributs = [];
        }
        
        // J'ai essayé d'acceder à un attribut inaccessible 
        // Il est retourné depuis mon tableau d'attributs 
        public function __get($name){
            return $this->attributs[$name];
        }
    }

    // On cré un nouvel objet Session
    $session = new Session();
    $session->titi;
`````

- __set 
Detecte l'affectation d'une valeur à un attribut inexistant ou inaccessible 

`````php
<?php
    class Session{
        private $attributs;
        
        public function __construct(){
            $this->attributs = [];
        }
        
        public function __set($name, $value){
            $this->attributs[$name]=$value;
        }
    }
  
`````

- __isset : Est appelé quand on appel la méthode magique isset sur un attribut inaccessible ou inexistant


`````php
<?php
    class Session{
        private $attributs;
        
        public function __construct(){
            $this->attributs = [];
        }
        
        public function __isset($name){
           echo('Tu as fais un isset sur '.$name. ' qui nexiste pas');
        }
    }
  
    $session = new Session();
    // Je vais passer dans ma méthode magique ! 
    isset($ession->toto);
`````

- __unset : Similaire au isset mais avec la méthode php unset 




`````php
<?php
    class Session{
        private $attributs;
        
        public function __construct(){
            $this->attributs = [];
        }
        
        public function __unset($name){
           echo('Tu as appelé la fonction php unset sur '.$name. ' qui nexiste pas');
        }
    }
  
    $session = new Session();
    // Je vais passer dans ma méthode magique ! $name => "toto"
    unset($session->toto);
`````

__sleep 
Est appelé quand on appel la méthode serialize sur un objet
Principe de sérialisation : Permet de transformer un objet en chaine de caractères 

`````php
<?php
    class Session{
        private $attributs;
        
        public function __construct(){
            $this->attributs = [];
        }
        
        public function __sleep(){
            echo('Je vais dormir en chaine de caractère !');
        }
    }
  
    $session = new Session();
    // Je vais passer dans ma méthode magique !
    serialize($session);
`````

__wakeup
Elle est appelé quand on appel la méthode unserialize sur un objet 
Principe désérialization consiste à tranformer une chaine de caractère en objet 


`````php
<?php
    class Session{
        private $attributs;
        
        public function __construct(){
            $this->attributs = [];
        }
        
        public function __wakeup(){
            echo('Je me réveil j\'étais une chaine de caractères !');
        }
    }
  
    $session = new Session();
    $chaineDeCaractere = serialize($session);

    // Je vais passer dans ma méthode magique !
    $object = unserialize($chaineDeCaractere);
`````