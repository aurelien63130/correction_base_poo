<?php
    interface CrudInterface{
        public function create($elementACreer);
        public function getAll();
        public function getOne($id);
        public function delete($id);
        public function edit($element);
    }
?>