<?php
// Cette classe sera en charge de transformer des requêtes SQL en modèle objet
// Elle héritera de la classe DbManager ce qui lui permettra d'être connecté à notre BDD
// On pourra donc utiliser l'attribut $bdd de la classe parente DbManager
class CategoryManager extends DbManager implements CrudInterface {

    // Si j'ai un constructeur dans cette classe, ne pas oublier d'appeler le constructeur parent
    // Sinon il serait écrasé
    public function __construct(){
        parent::__construct();
    }

    // Cette mèthode execute une requête qui selectionne tous les enregistrements de notre BDD
    // Elle transforme ce tableau de résultat MySQL en tableau d'objet
    public function getAll(){
        // Je déclare un tableau vide
        $categories = [];
        // J'execute ma requête
        $req = $this->bdd->query("SELECT * FROM category ORDER BY ordre_affichage");
        $req->execute();
        $result = $req->fetchAll();

        // Je pars de mes resultats sous forme de tableau pour les transformer en objet de type category
        foreach ($result as $categ){
            // Pour faire cela j'utilise le constructeur de ma table category
            $categories[] = new Category($categ["id"], $categ["nom"], $categ["ordre_affichage"]);
        }

        // Je retourne mon tableau d'objet
        return $categories;
    }

    // Cette méthode prend en paramètre un id (l'id de l'objet que l'on souhaite retrouvé)
    public function getOne($id){
        // Elle execute une requête qui selectionne l'objet sous forme de tableau
        $query = $this->bdd->prepare("SELECT * FROM category WHERE id = :id");
        $query->execute(["id"=> $id]);
        $res = $query->fetch();

        // Elle retourne notre objet
        if(isset($res["id"])){
            return new Category($res["id"], $res["nom"], $res["ordre_affichage"]);
        } else {
            return  null;
        }

    }

    // Cette fonction prend en paramètre un objet de type category
    public function create($category){
        // Elle transforme cet objet en requête SQL
        $req = $this->bdd->prepare("INSERT INTO category (nom, ordre_affichage) VALUES (:nom, :ordre_affichage)");
        $req->execute([
            "nom"=> $category->getNom(),
            "ordre_affichage"=> $category->getOrdreAffichage()
        ]);
        // Elle met à jour l'id de la categorie créé !
        $category->setId($this->bdd->lastInsertId());

        // Elle retourne notre objet Category
        return $category;
    }

    // Fonction qui prend en paramètre un id (celui de la category à supprimer)
    public function delete($id){
        $element = $this->getOne($id);

        if(is_null($element)){

            $exception = new Exception("Impossible de supprimer, cet element n'est pas dans la base de donnée ...");
            var_dump($exception->getTrace());

            throw $exception;
        }

        // Ici notre requête préparée supprime l'objet qui à l'id passé en paramètre
        $req = $this->bdd->prepare("DELETE FROM category WHERE id = :id");
        $req->execute(["id"=> $id]);
    }

    // Nous avons une méthode qui prend en paramètre une category et qui va mettre à jour notre enregistrement en BDD
    public function edit($category){

        // Ma requête préparé fait un update de tous les champs de la category sur son id
        $req = $this->bdd->prepare("UPDATE category SET nom = :nom, ordre_affichage = :ordre_affichage WHERE id = :id");
        $req->execute([
            "nom"=> $category->getNom(),
            "ordre_affichage"=> $category->getOrdreAffichage(),
            "id"=> $category->getId()
        ]);
    }
}