<?php
    // Création d'une classe abstraite DbManager
    // Cette classe servira à se connecter à ma BDD
    abstract class DbManager{
        // J'ai un attribut protected
        // Il ne sera accessible seulement dans les classes qui héritent de DbManager
        protected $bdd;

        // Dans le constructeur de DbManager, je me connecte à ma BDD
        // Les classes qui hériteront de DbManager seront donc systématiquement
        // connecté à ma BDD
        // Attention si on a un constructeur qui n'appel pas le constructeur parent dans une classe
        // enfant nous ne serons plus connecté à la BDD. Le constructeur enfant est prioritaire
        // sur le constructeur parent (sinon utilisation de parent::)
        public function __construct(){
            try {
                $this->bdd = new PDO("mysql:dbname=demo_poo;host=database", 'root', 'tiger');
            } catch (\PDOException $e){
                // Envoyer au mail au dev pour lui dire qu'il a fait une bétise ou que quelqu'un à éteint le serveur de la BDD
                echo('Envoi de mail : '.$e->getMessage().' vas vérifier ce fichier STP : '.$e->getFile().' a la ligne '. $e->getLine());
                // Lancer l'erreur
                throw $e;
            }

        }
    }
?>